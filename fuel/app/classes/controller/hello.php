<?php

class Controller_Hello extends Controller
{
	public function get_index()
	{
		$name = Input::get('name', 'World');
		return 'Hello ' . $name . '!';
	}
}
