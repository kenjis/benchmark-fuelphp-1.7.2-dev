# FuelPHP 1.7.2-dev for Benchmarking

## Installation

~~~
$ mkdir ~/tmp
$ cd ~/tmp
$ git clone git@bitbucket.org:kenjis/benchmark-fuelphp-1.7.2-dev.git
$ cd benchmark-fuelphp-1.7.2-dev
$ composer install
~~~

~~~
$ cd /path/to/htdocs
$ ln -s ~/tmp/benchmark-fuelphp-1.7.2-dev/public/ fuel
~~~

## Benchmarking

~~~
$ siege -b -c 10 -t 3S http://localhost/fuel/hello?name=BEAR
~~~
